/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071 Sec 1
 *  Sunat Praphanwong 6088130 Sec 1
 *  Barameerak Koonmongkon 6088156 Sec 1
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class TFIDFSearcher extends Searcher
{	
    HashMap<String,Double> InvertDocFreqMap = new HashMap<>();
    HashMap<Document,Integer> Docmap = new HashMap<>();
    HashMap<String, Integer> Wordbag = new HashMap<>(); // Create for union value between all token in all document     
    HashMap<Integer, double[]> Vector = new HashMap<>();
    
    Comparator<SearchResult> DocIDcompare = (SearchResult o1, SearchResult o2) -> o1.getDocument().getId()-o2.getDocument().getId(); // Compare Search 1 and Search 2 by using Comparator
    
	public TFIDFSearcher(String docFilename) 
        {
		super(docFilename);
		/************* YOUR CODE HERE ******************/
		double TFIDF, TF ; // Create for using in TFIDF and IF
                int termID = 0 ;
                int DocID = 0 ;
                double size = super.documents.size();
                List<String> token;
                Set<String> tokenSet;
        
            for (Document doc : super.documents) 
            {
            /** Create TermDict and DocDict**/
            if(!Docmap.containsKey(doc))
            {
                Docmap.put(doc, DocID);
                DocID++;
            }
            token = doc.getTokens();
            for (String string : token) 
                {
                    if(!Wordbag.containsKey(string))
                        {
                            Wordbag.put(string, termID);
                            termID++;
                        }
                }
        }
        
        for (Document doc : super.documents) 
        {
            /** Find IDF by using For Loop **/
            tokenSet = new HashSet<>(doc.getTokens());
            for(String string: tokenSet)
            {
                if(!InvertDocFreqMap.containsKey(string))
                {
                    InvertDocFreqMap.put(string, 1.0);
                }
                else InvertDocFreqMap.replace(string, InvertDocFreqMap.get(string)+1.0);
            }
        }
        InvertDocFreqMap.keySet().forEach((string) -> { 
            InvertDocFreqMap.replace(string, Math.log10(1.0 + (size/InvertDocFreqMap.get(string)))); // Calculate with LogNormalization Base 10
        });
        
        for (Iterator<Document> it = documents.iterator(); it.hasNext();) 
        {
            Document doc = it.next();
            double vector[] = new double[Wordbag.size()];
            token = doc.getTokens();
                    for (String word : token) {
                        TF = Collections.frequency(token, word);
                        if(TF > 0) TF = 1 + Math.log10(TF);
                        TFIDF = InvertDocFreqMap.get(word) * TF; // TFIDF from IDF(Get value from word in token) * TF 
                        vector[Wordbag.get(word)] = TFIDF;
                    }
            Vector.put(Docmap.get(doc), vector);
            } 
	}
	
	@Override
	public List<SearchResult> search(String queryString, int k) {
		/************* YOUR CODE HERE ******************/
		List<String> StrArray = Searcher.tokenize(queryString);
                List<SearchResult> results = new LinkedList<>();
                List<SearchResult> NaNresults = new LinkedList<>();
                PriorityQueue<SearchResult> resultqueue = new PriorityQueue<>(DocIDcompare);
                SearchResult cosine;
                double TF = 0;
                double score = 0.0;
                double normd , normq , qd;
                double Freq;
                double q[], d[]; // Create for calculate Vector
                int docid , i ;
                
                // Use for loop to Calculate Query Vector 
                for(Document doc: Docmap.keySet())
                {
                    normd = 0 ; normq = 0; qd = 0;
                    docid = Docmap.get(doc);
                    d = Vector.get(docid);
                    q = new double[Wordbag.size()];
                    
                    for(String string: StrArray)
                    {
                        if(Wordbag.get(string) != null)
                        {
                            Freq = Collections.frequency(StrArray, string); // Get the frequency of element from tokenize query as number
                            TF = 1 + Math.log10(Freq);
                            q[Wordbag.get(string)] = TF*InvertDocFreqMap.get(string); // TF-IDF = TF * IDF
                        }
                    }
                    for(i = 0 ; i < d.length ; i++)
                    {
                        qd += d[i] * q[i];
                        normd += Math.pow(d[i], 2);
                        normq += Math.pow(q[i], 2);
                    }
                    normd = Math.sqrt(normd);
                    normq = Math.sqrt(normq);
                    score = qd/(normd*normq); // Find score of Cosine
                    cosine = new SearchResult(doc, score); // Get the result of Cosine Similarity
                    if(Double.isNaN(score)) // if Score is not a number(NaN)
                    {
                        resultqueue.add(cosine);
                    }
                    else
                        results.add(cosine);
                }
                if(results.size()> 0) // Least result should be k items
                {
                    Collections.sort(results);  
                    return results.subList(0, k);
                }
                else // If we found Not a number result
                {
                     for(i = 0 ; i < k ; i++)
                         NaNresults.add(resultqueue.poll()); // retrieves and remove value out
                     return NaNresults.subList(0, k);

                } 
		/***********************************************/
	}
}
