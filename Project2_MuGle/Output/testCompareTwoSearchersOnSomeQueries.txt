run:
@@@ Comparing two searchers on some queries in ./data/lisa
@@@ Finished loading 35 documents from ./data/lisa/queries.txt
@@@ Finished loading 5999 documents from ./data/lisa/documents.txt
@@@ Finished loading 5999 documents from ./data/lisa/documents.txt
@@@ Query: [ID:1, I AM INTERESTED IN THE IDENTIFICATION AND EVALUATI...]
	Jaccard (P,R,F): [0.1, 0.5, 0.16666666666666669]
	TFIDF (P,R,F): [0.2, 1.0, 0.33333333333333337]
@@@ Query: [ID:18, I WOULD BE PLEASED TO RECEIVE ANY INFORMATION ON T...]
	Jaccard (P,R,F): [0.7, 0.1320754716981132, 0.22222222222222224]
	TFIDF (P,R,F): [0.9, 0.16981132075471697, 0.2857142857142857]
@@@ Query: [ID:35, I AM INTERESTED IN ALMOST ANYTHING TO DO WITH OCCU...]
	Jaccard (P,R,F): [0.0, 0.0, 0.0]
	TFIDF (P,R,F): [0.3, 0.42857142857142855, 0.3529411764705882]
@@@ Total time used: 3394 milliseconds.
BUILD SUCCESSFUL (total time: 3 seconds)
