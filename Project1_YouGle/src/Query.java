/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Consumer;

public class Query {

	// Term id -> position in index file
	private  Map<Integer, Long> posDict = new TreeMap<Integer, Long>();
	// Term id -> document frequency
	private  Map<Integer, Integer> freqDict = new TreeMap<Integer, Integer>();
	// Doc id -> doc name dictionary
	private  Map<Integer, String> docDict = new TreeMap<Integer, String>();
	// Term -> term id dictionary
	private  Map<String, Integer> termDict = new TreeMap<String, Integer>();
	// Index
	private  BaseIndex index = null;
	private static Index runindex = null;


	//indicate whether the query service is running or not
	private boolean running = false;
	private RandomAccessFile indexFile = null;

	/*
	 * Read a posting list with a given termID from the file
	 * You should seek to the file position of this specific
	 * posting list and read it back.
	 * */
	private  PostingList readPosting(FileChannel fc, int termId)throws IOException 
        {
		
            fc.position(posDict.get(termId));
            return index.readPosting(fc);
            /*
             * TODO: Your code here
            */
	}


	public void runQueryService(String indexMode, String indexDirname) throws IOException
	{
		//Get the index reader
		try 
                {
			Class<?> indexClass = Class.forName(indexMode+"Index");
			index = (BaseIndex) indexClass.newInstance();
		} 
                catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) 
                {
			System.err.println("Index method must be \"Basic\", \"VB\", or \"Gamma\"");
			throw new RuntimeException(e);
		}

		//Get Index file
		File inputdir = new File(indexDirname);
		if (!inputdir.exists() || !inputdir.isDirectory()) 
                {
			System.err.println("Invalid index directory: " + indexDirname);
			return;
		}

		/* Index file */
		indexFile = new RandomAccessFile(new File(indexDirname,"corpus.index"), "r");

		String line = null;
            try (BufferedReader termReader = new BufferedReader(new FileReader(new File(indexDirname, "term.dict")))) 
            {
                while ((line = termReader.readLine()) != null) 
                {
                    String[] tokens = line.split("\t");
                    termDict.put(tokens[0], Integer.parseInt(tokens[1]));
                }
            }

            try ( /* Doc dictionary */ 
                    BufferedReader docReader = new BufferedReader(new FileReader(new File(
                    indexDirname, "doc.dict")))) 
            {
                while ((line = docReader.readLine()) != null) 
                {
                    String[] tokens = line.split("\t");
                    docDict.put(Integer.parseInt(tokens[1]), tokens[0]);
                }
            }

            try ( /* Posting dictionary */ 
                    BufferedReader postReader = new BufferedReader(new FileReader(new File(
                    indexDirname, "posting.dict")))) 
            {
                while ((line = postReader.readLine()) != null) 
                {
                    String[] tokens = line.split("\t");
                    posDict.put(Integer.parseInt(tokens[0]), Long.parseLong(tokens[1]));
                    freqDict.put(Integer.parseInt(tokens[0]),
                            Integer.parseInt(tokens[2]));
                }
            }
		this.running = true;
	}

	public List<Integer> retrieve(String query) throws IOException
	{	
            if(!running)
	{
		System.err.println("Error: Query service must be initiated");
	}

		/*
		 * TODO: Your code here
		 *       Perform query processing with the inverted index.
		 *       return the list of IDs of the documents that match the query
		 *
		 */
		String[] tokens = query.split("\\s+");
		List<List<Integer>> list = new ArrayList<>();
		for(String token : tokens)
                {
			if(termDict.containsKey(token))
                        {
				list.add(readPosting(indexFile.getChannel(), termDict.get(token)).getList());
			}
		}
		Iterator<List<Integer>> iter = list.iterator();
		List<Integer> list1 = popNextOrNull(iter);
		List<Integer> l2;
//                System.out.println(list1);
		while (list1 != null && (l2 = popNextOrNull(iter)) != null)
			list1 = intersect(list1, l2);
		return list1;
	}

	private List<Integer> intersect (List<Integer> a, List<Integer> b) 
        {
		Iterator<Integer> it1 = a.iterator();
		Iterator<Integer> it2 = b.iterator();

		List<Integer> newList = new ArrayList<>();

		Integer l1 = popNextOrNull(it1);
		Integer l2 = popNextOrNull(it2);

		while (l1 != null && l2 != null) 
                {
                    if (Objects.equals(l1, l2)) 
                        {
                            newList.add(l1);
                            l1 = popNextOrNull(it1);
                            l2 = popNextOrNull(it2);
			} 
                    else if (l1 < l2) 
                        {
                            l1 = popNextOrNull(it1);
			} 
                    else 
                        {
                            l2 = popNextOrNull(it2);
			}
		}
		return newList;
	}

	private static <X> X popNextOrNull(Iterator<X> iter) 
        {
		if (iter.hasNext()) 
                {
			return iter.next();
		} 
                else 
                {
			return null;
		}
	}
	String outputQueryResult(List<Integer> res) {
		/*
		 * TODO:
		 *
		 * Take the list of documents ID and prepare the search results, sorted by lexicon order.
		 *
		 * E.g.
		 * 	0/fine.txt
		 *	0/hello.txt
		 *	1/bye.txt
		 *	2/fine.txt
		 *	2/hello.txt
		 *
		 * If there no matched document, output:
		 *
		 * no results found
		 *
		 * */

		StringBuilder str = new StringBuilder("");
		if(res == null)
                {
			return("NO RESULTS FOUND");
		}
                res.forEach((Integer docId) -> {
                    str.append(docDict.get(docId)).append("\n");
                });

		return str.toString();
	}

	public static void main(String[] args) throws IOException 
        {
		/* Parse command line */
		if (args.length != 2) 
                {
			System.err.println("Usage: java Query [Basic|VB|Gamma] index_dir");
			return;
		}

		/* Get index */
		String className = null;
		try 
                {
			className = args[0];
		}
                catch (Exception e) 
                {
			System.err.println("Index method must be \"Basic\", \"VB\", or \"Gamma\"");
			throw new RuntimeException(e);
		}

		/* Get index directory */
		String input = args[1];

		Query queryService = new Query();
		queryService.runQueryService(className, input);

            /* For each query */
            try ( /* Processing queries */ 
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) 
            {
                /* For each query */
                String line = null;
                while ((line = br.readLine()) != null) 
                {
                    List<Integer> hitDocs = queryService.retrieve(line);
                    queryService.outputQueryResult(hitDocs);
                }
            }
	}

        @Override
	protected void finalize() throws Throwable
	{
            try 
                {
                    if(indexFile != null)indexFile.close();
		} 
            catch (IOException e) 
            {
            } 
            finally 
            {
                super.finalize();
            }
	}
}
/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 **/
