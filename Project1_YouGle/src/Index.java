/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 *  Environment NetBeans IDE 8.2 with JRE 1.8.0
 **/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.Map;
import java.util.TreeMap;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class Index 
{
	// Term id -> (position in index file, doc frequency) dictionary
	private static Map<Integer, Pair<Long, Integer>> postingDict
			= new TreeMap<Integer, Pair<Long, Integer>>();
	// Doc name -> doc id dictionary
	private static Map<String, Integer> docDict
			= new TreeMap<String, Integer>();
	// Term -> term id dictionary
	private static Map<String, Integer> termDict
			= new TreeMap<String, Integer>();
	// Block queue
	private static LinkedList<File> blockQueue
			= new LinkedList<File>();

	// Total file counter
	private static int totalFileCount = 0;
	// Document counter
	private static int docIdCounter = 0;
	// Term counter
	private static int wordIdCounter = 0;
	// Index
	private static BaseIndex index = null;

	private static long sPoint=0;
	/*
	 * Write a posting list to the given file
	 * You should record the file position of this posting list
	 * so that you can read it back during retrieval
	 *
	 * */
	private static void writePosting(FileChannel fc, PostingList posting)throws IOException 
        {
		/*
		 * TODO: Your code here
		 *
		 */
		int frequency=posting.getList().size();
		int termId = posting.getTermId();
		index.writePosting(fc,posting);
		Pair<Long, Integer > order = new Pair<>(sPoint,frequency);
		postingDict.put(termId, order);
		sPoint += 8+ (frequency*4); // check between fc.position;
	}
	/**
	 * Pop next element if there is one, otherwise return null
	 * @param iter an iterator that contains integers
	 * @return next element or null
	 */
	private static Integer popNextOrNull(Iterator<Integer> iter) 
        {
		if (iter.hasNext()) 
                {
			return iter.next();
		} 
                else 
                {
			return null;
		}
	}
	/**
	 * Main method to start the indexing process.
	 * @param method		:Indexing method. "Basic" by default, but extra credit will be given for those
	 * 			who can implement variable byte (VB) or Gamma index compression algorithm
	 * @param dataDirname	:relative path to the dataset root directory. E.g. "./datasets/small"
	 * @param outputDirname	:relative path to the output directory to store index. You must not assume
	 * 			that this directory exist. If it does, you must clear out the content before indexing.
         * @return 
         * @throws java.io.IOException
	 */
	public static int runIndexer(String method, String dataDirname, String outputDirname) throws IOException
	{
		/* Get index */
		String className = method + "Index";
		try 
                {
			Class<?> indexClass = Class.forName(className);
			index = (BaseIndex) indexClass.newInstance();
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) 
                {
			System.err.println("Index method must be \"Basic\", \"VB\", or \"Gamma\"");
			throw new RuntimeException(e);
		}

		/* Get root directory */
		File rootdir = new File(dataDirname);
		if (!rootdir.exists() || !rootdir.isDirectory()) 
                {
			System.err.println("Invalid data directory: " + dataDirname);
			return -1;
		}


		/* Get output directory*/
		File outdir = new File(outputDirname);
		if (outdir.exists() && !outdir.isDirectory()) 
                {
			System.err.println("Invalid output directory: " + outputDirname);
			return -1;
		}

		/*	
                TODO: delete all the files/sub folder under outdir
		 */
//                if(outdir.isFile())
//		{
//		 	for(File fi: outdir.listFiles())
//		 	{
//		 		fi.delete();
//		 	}
//		}
		delete(outdir); // send to function to remove is slower a little bit but 100% rem. completely
		if (!outdir.exists() && !outdir.mkdirs()) 
                {
                    System.err.println("Create output directory failure");
                    return -1;
                }

		/* BSBI indexing algorithm */
		File[] dirlist = rootdir.listFiles();

		wordIdCounter =0;

		/* For each block */
		for (File block : dirlist) {
			Map<Integer,PostingList> posting = new TreeMap<>();//id+docid
			File blockFile = new File(outputDirname, block.getName());
			System.out.println("Processing block "+block.getName());
			blockQueue.add(blockFile);

			File blockDir = new File(dataDirname, block.getName());
			File[] filelist = blockDir.listFiles();

			/* For each file */
			for (File file : filelist) 
                        {
				++totalFileCount;
				String fileName = block.getName() + "/" + file.getName();
				// use pre-increment to ensure docID > 0
				int docId = ++docIdCounter;
				docDict.put(fileName, docId);
                            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                                String line;
                                while ((line = reader.readLine()) != null) 
                                {
                                    String[] tokens = line.trim().split("\\s+");
                                    for (String token : tokens) 
                                    {
                                        /*
                                        * TODO: Your code here
                                        *       For each term, build up a list of
                                        *       documents in which the term occurs
                                        */
                                        if(!termDict.containsKey(token))
                                        {
                                            wordIdCounter++;
                                            termDict.put(token, wordIdCounter);
                                        }
                                        if(posting.containsKey(termDict.get(token)))
                                        {
                                            if(!posting.get(termDict.get(token)).getList().contains(docId))
                                            {                               
                                                posting.get(termDict.get(token)).getList().add(docId);
                                            }
                                        }
                                        else if(!posting.containsKey(termDict.get(token)))
                                        {
                                            PostingList newPost = new PostingList(termDict.get(token));
                                            newPost.getList().add(docId);
                                            posting.put(termDict.get(token), newPost);
                                        }                  
                                    }
                                }
                            }
			}
			/* Sort and output */
			if (!blockFile.createNewFile()) 
                        {
				System.err.println("Create new block failure.");
				return -1;
			}
                    /*
                     * TODO: Your code here
                     *       Write all posting lists for all terms to file (bfc)
                     */
                    try (RandomAccessFile bfc = new RandomAccessFile(blockFile, "rw")) {

                        FileChannel fileCh = bfc.getChannel();
                        int specialcheck=0;
                        for(int i: posting.keySet())
                        {
                            specialcheck=1;
                            writePosting(fileCh, posting.get(i));
                        }
                        if(specialcheck==1)
                        {
                            System.out.println("checking");
                        }
                    }
		}

		/* Required: output total number of files. */
		/* Merge blocks */
		while (true) {
			if (blockQueue.size() <= 1)
				break;

			File b1 = blockQueue.removeFirst();
			File b2 = blockQueue.removeFirst();

			File combfile = new File(outputDirname, b1.getName() + "+" + b2.getName());
			if (!combfile.createNewFile()) 
                        {
				System.err.println("Create new block failure.");
				return -1;
			}

                        RandomAccessFile bf2;
                        RandomAccessFile mf;
                    try (RandomAccessFile bf1 = new RandomAccessFile(b1, "r")) 
                    {
                        bf2 = new RandomAccessFile(b2, "r");
                        mf = new RandomAccessFile(combfile, "rw");
                        /*
                        * TODO: Your code here
                        *       Combine blocks bf1 and bf2 into our combined file, mf
                        *       You will want to consider in what order to merge
                        *       the two blocks (based on term ID, perhaps?).
                        *
                        */
                        long position =0; //check to use file.getposition
                        PostingList posRead = index.readPosting(bf1.getChannel().position(position));
                        Map<Integer,PostingList> postingMerge = new TreeMap<>();//id+docid
                        while(posRead!=null)
                        {
                            postingMerge.put(posRead.getTermId(),posRead);// input data to treeMap from 1 then add 2 to it
                            position += 8+(4*posRead.getList().size());
                            posRead = index.readPosting(bf1.getChannel().position(position));
                        }
                        position = 0;
                        posRead = index.readPosting(bf2.getChannel().position(position));
                        while(posRead!=null){
                            if(postingMerge.containsKey(posRead.getTermId()))
                            {
                                for(int i: posRead.getList()){
                                    if(!postingMerge.get(posRead.getTermId()).getList().contains(i))
                                    {
                                        postingMerge.get(posRead.getTermId()).getList().add(i);
                                    }
                                }          
                            }
                            else {
                                postingMerge.put(posRead.getTermId(),posRead);
                            }
                        position += 8+(4*posRead.getList().size());
                        posRead = index.readPosting(bf2.getChannel().position(position));
                        }
                        sPoint = 0;
                        for(int i: postingMerge.keySet()){
                            Collections.sort(postingMerge.get(i).getList());
                            writePosting(mf.getChannel(), postingMerge.get(i));
                        }
                    }
			bf2.close();
			mf.close();
			b1.delete();
			b2.delete();
			blockQueue.add(combfile);
		}

		/* Dump constructed index back into file system */
		File indexFile = blockQueue.removeFirst();
		indexFile.renameTo(new File(outputDirname, "corpus.index"));

            try (BufferedWriter termWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "term.dict")))) 
            {
                for (String term : termDict.keySet()) 
                {
                    termWriter.write(term + "\t" + termDict.get(term) + "\n");
                }
            }

            try (BufferedWriter docWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "doc.dict")))) 
            {
                for (String doc : docDict.keySet()) 
                {
                    docWriter.write(doc + "\t" + docDict.get(doc) + "\n");
                }
            }

            try (BufferedWriter postWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "posting.dict")))) 
            {
                for (Integer termId : postingDict.keySet()) 
                {
                    postWriter.write(termId + "\t" + postingDict.get(termId).getFirst()
                            + "\t" + postingDict.get(termId).getSecond() + "\n");
                }
            }

		return totalFileCount;
	}

	private static void delete(File deleteFile) 
        {
		// TODO Auto-generated method stub
		if(deleteFile.isFile())
                {
			for(File fi : deleteFile.listFiles()) // check if file is left
                        {
				fi.delete(); // then delelte files
			}
		}
	}

	public static void main(String[] args) throws IOException {
		/* Parse command line */
		if (args.length != 3) {
			System.err.println("Usage: java Index [Basic|VB|Gamma] data_dir output_dir");
			return;
		}
		/* Get index */
		String className = "";
		try {
			className = args[0];
		} catch (Exception e) 
                {
			System.err.println("Index method must be \"Basic\", \"VB\", or \"Gamma\"");
			throw new RuntimeException(e);
		}
		/* Get root directory */
		String root = args[1];
		/* Get output directory */
		String output = args[2];
		runIndexer(className, root, output);
	}
}
/**
 *  Poonkasem Kasemsri Na Ayutthaya 6088071
 *  Sunat Praphanwong 6088130
 *  Barameerak Koonmongkon 6088156
 **/
