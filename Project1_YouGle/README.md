# Project 1 YouGle: Your First Search Engine

## Deadline : Friday September 23, 2019 11:55PM

### เราต้องโหลดอะไรบ้าง

-   โค้ดของอาจารย์
-   ชุดข้อมูลหรือ Dataset (Download here: https://goo.gl/AZeRJh) โหลดแล้วคลายไฟล์ด้วย 7-Zip(ดีกว่า WinRAR)ลงในแฟ้มที่สร้างโปรเจค

โครงสร้างไฟล์โปรเจคจะมีหน้าตาแบบนี้

```bash
p1_package
├── datasets
├── index
│   ├── citeseer
│   │   ├── **/*.dict , stats.txt
│   ├── large
|   |   ├── **/*.dict , stats.txt
│   ├── small
|   |   ├── **/*.dict , stats.txt
├── output
|   ├── citeseer
|   |   ├── **/*.out , stats.txt
|   ├── large
|   |   ├── **/*.out , stats.txt
|   ├── small
|   |   ├── **/*.out , stats.txt
├── src
|   ├── BaseIndex.java
|   ├── BasicIndex.java
|   ├── Index.java
|   ├── P1Tester.java
|   ├── Pair.java
|   ├── PostingList.java
|   ├── Query.java
├── .classpath
├── .project
├── build.xml
```

ชุดข้อมูลประกอบด้วยอะไรบ้าง
- small
- large
- citeseer


## มีกี่ TODO?

มีทั้งหมด 10 TODO 
- สำหรับไฟล์ BasicIndex.java มี 2 TODO
  - ฟังก์ชัน ReadPosting(FileChannel fc). Read and return the postings list from the given file.
   ```java
        /*
		 * TODO: Your code here
		 * Read and return the postings list from the given file.
		 */
    ```
  - ฟังก์ชัน writePosting(FileChannel fc, PostingList p). Write the given postings list to the given file.
  ```java
        /*
		 * TODO: Your code here
		 * Write the given postings list to the given file.
		 */
  ```
- สำหรับไฟล์ Index.java มี 5 TODO
  - ฟังก์ชัน writePosting(FileChannel fc, PostingList posting)
```java
	/*
	* TODO: Your code here 
	*/
	/**
    * Pop next element if there is one, otherwise return null
    * @param iter an iterator that contains integers
    * @return next element or null
    */
```
  - runIndexer(String method, String dataDirname, String outputDirname) throws IOException 
```java
	/*	TODO: delete all the files/sub folder under outdir
    */
    while ((line = reader.readLine()) != null) 
    {
	String[] tokens = line.trim().split("\\s+");
    for (String token : tokens) 
    {
	/*
	 * TODO: Your code here
	*       For each term, build up a list of
	*       documents in which the term occurs
	*/
	}
}

	RandomAccessFile bfc = new RandomAccessFile(blockFile, "rw");
			
	/*
	 * TODO: Your code here
	 *       Write all posting lists for all terms to file (bfc) 
	 */
    bfc.close();
    RandomAccessFile bf1 = new RandomAccessFile(b1, "r");
	RandomAccessFile bf2 = new RandomAccessFile(b2, "r");
	RandomAccessFile mf = new RandomAccessFile(combfile, "rw");	 
	/*
	* TODO: Your code here
	*       Combine blocks bf1 and bf2 into our combined file, mf
	*       You will want to consider in what order to merge
	*       the two blocks (based on term ID, perhaps?).
	*       
	*/
```
- สำหรับไฟล์ Query.java มี 3 TODO
  - ฟังก์ชัน 	private  PostingList readPosting(FileChannel fc, int termId)
			throws IOException 
```java
    /* 
	 * Read a posting list with a given termID from the file 
	 * You should seek to the file position of this specific
	 * posting list and read it back.
	 * */
	private  PostingList readPosting(FileChannel fc, int termId) throws IOException {
		/*
		 * TODO: Your code here
		 */
		return null;
	}
```
  -   ฟังก์ชัน public List<Integer> retrieve(String query) throws IOException
```java
	public List<Integer> retrieve(String query) throws IOException
	{	if(!running) 
		{
			System.err.println("Error: Query service must be initiated");
		}
		
		/*
		 * TODO: Your code here
		 *       Perform query processing with the inverted index.
		 *       return the list of IDs of the documents that match the query
		 *      
		 */
		return null;	
    }
    String outputQueryResult(List<Integer> res) {
        /*
         * TODO: 
         * 
         * Take the list of documents ID and prepare the search results, sorted by lexicon order. 
         * 
         * E.g.
         * 	0/fine.txt
		 *	0/hello.txt
		 *	1/bye.txt
		 *	2/fine.txt
		 *	2/hello.txt
		 *
		 * If there no matched document, output:
		 * 
		 * no results found
		 * 
         * */
    	
    	return null;
    }
```
<br>

## เริ่มที่ ไฟล์ BasicIndex.java
เราจะต้อง import lib อะไรบ้าง
```java
import java.io.IOException; // เอาไว้เปิดการ์ดยกเว้น error ของ I/O ที่เกิดขึ้นทุกอย่าง
import java.nio.ByteBuffer; // เป็น lib สำหรับเรียกค่า bytebuffer มาใช้เป็นหน่วยความจำชั่วคราว
import java.util.ArrayList; // เรียกมาใช้ทำอาเรย์นั่นแหละ
```

### เริ่มที่ไฟล์ BasicIndex.java ก่อน 

ที่ฟังก์ชัน ReadPosting(FileChannel fc)
```java
private static final int INT_BYTES = Integer.SIZE / Byte.SIZE;
@Override
public PostingList readPosting(FileChannel fc) 
    {
        // ให้เราทำการสร้างตัวแปรขึ้นมาเก็บค่าสองตัว, PostingList และก็ ByteBuffer สำหรับฟังก์ชันนี้
        int wordID , checkfreq;
        ByteBuffer bf = = ByteBuffer.allocate(4); // จะกำหนด 4 เป็นขั้นต่ำ หรือใช้ตัว INT_BYTES ก็ได้
        // ตัว ByteBuffer นั้นมี สองแบบคือ allocate แบบธรรมดา กับ allocateDirect ซึ่ง allocateDirect นั้นจะจอง buffer ลงใน I/O เลยซึ่งมีความเร็วที่สูงกว่าทั่วไปด้วย ซึ่ง Bytebuffer นั้นเป็นการอนุญาตให้ตัว dataset ที่เปิดขึ้นมานำไปเก็บใน buffer ก่อนจากนั้นค่อยนำไปใช้งานต่อ
        PostingList simulatePosting = null; // PostingList เป็นรูปแบบอาเรย์ชนิดหนึ่ง
        try { // try-catch คือการลองพยายามทำอะไรให้เกิดขึ้นก่อน ซึ่ง try จะเกิดก่อนและตามมาด้วย catch เสมอ catch เป็นการจับผลลัพธ์ที่ผิดพลาดออกมาในรูปแบบ Error หรือ e.printStackTrace();
        if(fc.size()>fc.position())
            {
            int ReadByte = fc.read(bf); // อ่านค่าแล้วนำลงไปใส่ใน buffer
            bf.rewind(); // เป็นการกรอ buffer เพื่อกลับไปใช้ใหม่
            wordID = bf.getInt(); // buffer มารับค่าในรูปแบบ integer
			bf.clear(); // เคลียร์ค่าใน buffer ทิ้งไป
			simulatePosting = new PostingList(wordID); // สร้างตัว PostingList ที่มีการเอา buffer ที่รับค่า int มาใช้
			ReadByte = fc.read(bf); // อ่านค่าใน buffer
			bf.rewind();
			checkfreq = bf.getInt();
			bf.clear();
			int sizeLoop = checkfreq; // สร้างตัวแปร มาเป็นขนาดลูป
			for(int i=0; i<sizeLoop; i++)
			{
                ReadByte = fc.read(bf);
                bf.rewind();
                int docId = bf.getInt();
                simulatePosting.getList().add(docId); // เอาค่าจาก docId เพิ่มเข้าไปใน PostingList ที่ชื่อ simulatePosting
                bf.clear(); 
			}
	    }
        else
        {
            return null;
		}
    }
        catch(IOExecption e)
        {
            e.printStackTrace(); // เอาไว้ใช้หา Error จาก try {}
        }
	return simulatePosting;
}
```

ต่อมาที่ writePosting(FileChannel fc, PostingList p)

```java
@Override
	public void writePosting(FileChannel fc, PostingList p)
        {
        /*
        เราต้องสร้างเหมือนกับฟังก์ชันข้างบนแต่จะใช้เป็น ArrayList แทนและยังต้องใช้ try-catch อีกเหมือนเดิม
        */
        ArrayList<Integer> num = new ArrayList<>(); // สร้าง ArrayList ที่เก็บแค่ค่า Integer โดยเฉพาะ
		ByteBuffer Bytebuff = ByteBuffer.allocateDirect(INT_BYTES*(p.getList().size()+2)); // Maximum Buffer into I/O routines นำค่า INT_BYTES ที่ได้สร้างไว้มาใช้กับ PostingList เพื่อกำหนดขนาด buffer
		num.add(p.getTermId()); // เอาค่าจาก PostingList ใส่ลงใน ArrayList
		num.add(p.getList().size()); // เอาขนาดของ PostingList ใส่ลงใน ArrayList
		for(int i = 0; i < p.getList().size(); i++)
        {
            num.add(p.getList().get(i)); // เอาขนาดของ PostingList ใส่ลงใน ArrayList โดยใช้ i มาจัดลำดับ
		}
		int j=num.size(); // เอา j มาเป็นตัวบอกขนาดของ arraylist ชื่อ Num
		Integer[] gS = new Integer[j];
		gS = num.toArray(gS); // ส่งค่าจาก ArrayList ไป Array ปกติ
        for (Integer i : gS) 
        {
            Bytebuff.putInt(i); // เอาค่าที่ได้ใส่ลงใน bytebuffer
        }
		Bytebuff.rewind();
		try 
        {
            fc.write(Bytebuff); // เขียนข้อมูลลง buffer
            Bytebuff.clear();

        } 
        catch (IOException e) 
        {
        e.printStackTrace();
		}
	}
}
```

### ต่อด้วยไฟล์ Index.java

เริ่มที่ TODO อันแรกตรง
```java
    private static long sPoint=0; // สร้างขึ้นมาด้วย
    private static void writePosting(FileChannel fc, PostingList posting)throws IOException 
    {
		/*
		 * TODO: Your code here
		 *	 
		 */
		
	}
	 /* 
	 * Write a posting list to the given file 
	 * You should record the file position of this posting list
	 * so that you can read it back during retrieval
	 * 
	 * เราจะต้องทำการเอาค่าที่ได้จาก dataset เขียนลงใน PostingList และควรเขียนตำแหน่งระหว่างที่เขียนค่าด้วย
     *
     */
    
    // สร้างตัวแปรเพื่อมารับค่าของ PostingList และขนาดของ PostingList
    int a = posting.getList().size();
    int b = posting.getTermId();
    index.writePosting(fc,posting);
    Pair<Long,Integer> order = new Pair<Long,Integer>(sPoint,a); // สร้าง Pair มาเพื่อจับคู่ระหว่าง dataset สองชุดที่มี Key เป็น Long และ Value เป็น Integer
    postingDict.put(b,order); // เอาค่า b และ Pair ใส่ลงใน Map ชื่อ postingDict
    sPoint += (8 + (a * 4));
```

ต่อที่ฟังก์ชั่น public static int runIndexer(String method, String dataDirname, String outputDirname) throws IOException

```java
	private static void delete(File deleteFile) 
        {
		// TODO Auto-generated method stub
		if(deleteFile.isFile())
                {
			for(File fi : deleteFile.listFiles())
                        {
				fi.delete();
			}
		}
	}
// แล้วเรียกชื่อ delete(ชื่อตัวแปรที่ต้องการลบค่า)
```

```java
	for (String token : tokens) 
	{
	/*
	 * TODO: Your code here
	 *       For each term, build up a list of
	 *       documents in which the term occurs
     */
	}

```

เริ่มเขียนใน for-loop 
```java
public static int runIndexer(String method, String dataDirname, String outputDirname) throws IOException
{
	/* Get index */
	String className = method + "Index";
	try 
    {
		Class<?> indexClass = Class.forName(className);
		index = (BaseIndex) indexClass.newInstance();
	} 
	catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) 
    {
		System.err.println("Index method must be \"Basic\", \"VB\", or \"Gamma\"");
	throw new RuntimeException(e);
	}
		/* Get root directory */
	File rootdir = new File(dataDirname);
	if (!rootdir.exists() || !rootdir.isDirectory()) 
    {
		System.err.println("Invalid data directory: " + dataDirname);
		return -1;
	}
		/* Get output directory*/
	File outdir = new File(outputDirname);
	if (outdir.exists() && !outdir.isDirectory()) 
    {
		System.err.println("Invalid output directory: " + outputDirname);
		return -1;
	}
	/*TODO: delete all the files/sub folder under outdir*/
	if(outdir.isFile())
	{
		for(File fi: outdir.listFiles())
		{
			fi.delete();
		}
	}
	if (!outdir.exists() && !outdir.mkdirs()) 
        {
            System.err.println("Create output directory failure");
            return -1;
		}

		/* BSBI indexing algorithm */
	File[] dirlist = rootdir.listFiles();

	wordIdCounter =0;

		/* For each block */
	for (File block : dirlist) 
	{
		Map<Integer,PostingList> posting = new TreeMap<>(); //สร้าง Map ขึ้นมา เพื่อเก็บid+docid
		File blockFile = new File(outputDirname, block.getName());
		System.out.println("Processing block "+block.getName());
		blockQueue.add(blockFile);

		File blockDir = new File(dataDirname, block.getName());
		File[] filelist = blockDir.listFiles();

		/* For each file */
	for (File file : filelist) 
    {
	++totalFileCount;
	String fileName = block.getName() + "/" + file.getName();
	// use pre-increment to ensure docID > 0
	int docId = ++docIdCounter;
	docDict.put(fileName, docId);
	try (BufferedReader reader = new BufferedReader(new FileReader(file))) 
		{
    		String line;
    		while ((line = reader.readLine()) != null) 
    	{
    String[] tokens = line.trim().split("\\s+");
    for (String token : tokens) 
    {
    /*
    * TODO: Your code here
    * For each term, build up a list of
    * documents in which the term occurs
    */
    if(!termDict.containsKey(token)) // ถ้า Map ชื่อ termDict ไม่มี token 
    	{
    	wordIdCounter++; // wordIdCounter เพิ่มค่า
		termDict.put(token, wordIdCounter); // เอา token, wordIdCounter ลง Map ที่ชื่อ termDict
    	}
    if(posting.containsKey(termDict.get(token))) // ถ้า posting มี termDict ที่ได้รับค่า token เป็นรูปแบบ string
    {
    	if(!posting.get(termDict.get(token)).getList().contains(docId)) // ถ้าตัว posting.get(termDict.get(token)) คือ การรับค่าจาก termDict และ termDict นั้นรับค่า token ในรูปแบบ string อีกที จากนั้น posting จะไปเรียก list ทั้งหมดที่มีซึ่งมีค่า docId ในรูปแบบ int ซึ่งถ้า posting นี้ไม่ได้รับค่า
    		{                               
    			posting.get(termDict.get(token)).getList().add(docId); // ตัว posting.get(termDict.get(token)) คือ การรับค่าจาก termDict และ termDict นั้นรับค่า token ในรูปแบบ string อีกที จากนั้น posting จะไปเรียก list ทั้งหมดแล้วเอา docId เพิ่มลงไป
    		}
    }
    else if(!posting.containsKey(termDict.get(token))) // ถ้า posting ไม่มี termDict ที่ได้รับค่า token เป็นรูปแบบ string
    {
    PostingList newPost = new PostingList(termDict.get(token)); // สร้าง postinglist ใหม่มาเก็บค่าโดยเฉพาะ
    newPost.getList().add(docId); // เอา postinglist ที่สร้างเมื่อกี้ เพิ่ม docId ลงไป
    posting.put(termDict.get(token), newPost); // เอา termDict กับ newpost ใส่ลงใน posting 
    			}                  
    		}
    	}
    }
	}
			/* Sort and output */
	if (!blockFile.createNewFile()) 
    {
		System.err.println("Create new block failure.");
		return -1;
	}
    /*
     * TODO: Your code here
     *       Write all posting lists for all terms to file (bfc)
     */
	try (RandomAccessFile bfc = new RandomAccessFile(blockFile, "rw")) 
	{
		FileChannel fileCh = bfc.getChannel(); // เรียกตำแหน่งตัวอักษรที่อยู่ในไฟล์ออกมา
        int specialcheck=0;
        for(int i: posting.keySet()) // for loop นี้จะรันไปเรื่อยๆจน posting ไม่มีค่าแล้ว
        {
            specialcheck=1;
            writePosting(fileCh, posting.get(i)); // เรียกฟังก์ชัน writePosting(fileCh -> เรียกตำแหน่งตัวอักษร, posting.get(i) -> เรียก posting ออกมาเป็นลำดับ)
        }
        if(specialcheck==1)
        {
            System.out.println("check");
        }
    }
	}
	/* Required: output total number of files. */
	/* Merge blocks */
	while (true) 
	{
		if (blockQueue.size() <= 1)
			break;

			File b1 = blockQueue.removeFirst();
			File b2 = blockQueue.removeFirst();

			File combfile = new File(outputDirname, b1.getName() + "+" + b2.getName());
			if (!combfile.createNewFile()) 
            {
				System.err.println("Create new block failure.");
				return -1;
			}
    RandomAccessFile bf2;
    RandomAccessFile mf;
    try (RandomAccessFile bf1 = new RandomAccessFile(b1, "r")) 
    {
    bf2 = new RandomAccessFile(b2, "r");
	mf = new RandomAccessFile(combfile, "rw");
    /*
    * TODO: Your code here
    *       Combine blocks bf1 and bf2 into our combined file, mf
    *       You will want to consider in what order to merge
    *       the two blocks (based on term ID, perhaps?).
    *
    */
    long position =0; // เอาไว้ใช้เพื่อเช็ค file.getposition
    PostingList posRead = index.readPosting(bf1.getChannel().position(position)); // สร้าง postinglist มาเก็บ RandomAcessFile
    Map<Integer,PostingList> postingMerge = new TreeMap<>(); // สร้างมาเก็บ id+docid
    while(posRead!=null) // ถ้า postinglist นั้นไม่ว่าง (คือมันมีค่าอยู่อ่ะ)
        {
            postingMerge.put(posRead.getTermId(),posRead);// เอา postinglist ที่เอาแค่ ID กับ postread ใส่ลงไปใน postingMerge (รวมค่า)
            position += 8+(4*posRead.getList().size());  
            posRead = index.readPosting(bf1.getChannel().position(position)); // PostingList จะทำการอ่านค่าจาก index(มาจาก BaseIndex) ที่อ่านค่าตำแหน่งไฟล์แบบสุ่ม
        }
    position = 0; 
    posRead = index.readPosting(bf2.getChannel().position(position)); // PostingList จะทำการอ่านค่าจาก index(มาจาก BaseIndex) ที่อ่านค่าตำแหน่งไฟล์แบบสุ่ม
	while(posRead!=null) // ถ้า PosRead ไม่ว่าง
	{
        if(postingMerge.containsKey(posRead.getTermId())) // ถ้า postingMerge มีคีย์จาก posRead ที่มี TermId
    {
		for(int i: posRead.getList())
	{
            if(!postingMerge.get(posRead.getTermId()).getList().contains(i)) // ถ้า postingMerge ไม่สามารถรับค่า (posRead) ได้
        	{
            postingMerge.get(posRead.getTermId()).getList().add(i); // postingMerge รับค่า posRead ที่มี TermId และรับ list จากนั้นเอา i ใส่ลงไป
        	}
    	}          
	}
	else 
	{
        postingMerge.put(posRead.getTermId(),posRead); // เอา posRead ที่มี TermId และ posRead ใส่ลงใน postingMerge
    }
    position += 8+(4*posRead.getList().size());
    posRead = index.readPosting(bf2.getChannel().position(position));
    }
    sPoint = 0;
	for(int i: postingMerge.keySet())
	{
        Collections.sort(postingMerge.get(i).getList());
        writePosting(mf.getChannel(), postingMerge.get(i));
    }
    }
	bf2.close();
	mf.close();
	b1.delete();
	b2.delete();
	blockQueue.add(combfile);
	}

	/* Dump constructed index back into file system */
	File indexFile = blockQueue.removeFirst();
	indexFile.renameTo(new File(outputDirname, "corpus.index"));

    try (BufferedWriter termWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "term.dict")))) 
        {
            for (String term : termDict.keySet()) 
            {
                termWriter.write(term + "\t" + termDict.get(term) + "\n");
            }
        }

    try (BufferedWriter docWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "doc.dict")))) 
        {
            for (String doc : docDict.keySet()) 
            {
                docWriter.write(doc + "\t" + docDict.get(doc) + "\n");
            }
        }

    try (BufferedWriter postWriter = new BufferedWriter(new FileWriter(new File(outputDirname, "posting.dict")))) 
        {
            for (Integer termId : postingDict.keySet()) 
            {
                postWriter.write(termId + "\t" + postingDict.get(termId).getFirst() + "\t" + postingDict.get(termId).getSecond() + "\n");
            }
        }

	return totalFileCount;
}
```

### สุดท้ายกับไฟล์ Query.java

เริ่มด้วยฟังก์ชัน private  PostingList readPosting(FileChannel fc, int termId)throws IOException {}

```java
private  PostingList readPosting(FileChannel fc, int termId)throws IOException 
	{
	/* 
	 * Read a posting list with a given termID from the file 
	 * You should seek to the file position of this specific
	 * posting list and read it back.
	 * */
	/* เราจะต้องทำให้ฟังก์ชันนี้อ่าน list จาก postinglist โดยมี termID จากไฟล์ที่เข้ามาและสามารถบอกตำแหน่งของไฟล์ที่อ่านได้อีกด้วย
		 * TODO: Your code here
		 */
		return null;
	}
```

```java
	private  PostingList readPosting(FileChannel fc, int termId) throws IOException 
	{
		fc.position(posDict.get(termId)); // ให้ FileChannel อ่าน ตำแหน่งจาก termID ของ posDict
        return index.readPosting(fc);	// รีเทิร์นค่าเป็น index ของ fc
	}
```

ต่อที่ฟังก์ชัน Public List<Integer> retrieve(String query) throws IOException{}

```java
public List<Integer> retrieve(String query) throws IOException
	{	if(!running) 
		{
			System.err.println("Error: Query service must be initiated");
		}
		
		/*
		 * TODO: Your code here
		 * Perform query processing with the inverted index.
		 * return the list of IDs of the documents that match the query
		 * เราต้องทำให้ query ที่เรียงกันมั่วตั้วนั้นเรียงออกมาให้ตรงกับ ID ของแต่ละตัวของไฟล์
		 */
		return null;
		
	}
```

```java
public List<Integer> retrieve(String query) throws IOException
{	
    if(!running)
	{
		System.err.println("Error: Query service must be initiated");
	}
	String[] tokens = query.split("\\s+"); // Regex ที่เอาไว้แบ่งช่องว่างจาก String ที่ชื่อ query หรือ split word with many whitespaces(ช่องว่าง) 
	List<List<Integer>> list = new ArrayList<>(); // NestedList หรือ arraylist ที่มีอาเรย์ซ้อนอีกที
	for(String token : tokens) // ถ้า for loop นี้สามารถแบ่งช่องว่างได้
    {
		if(termDict.containsKey(token)) // ถ้า termDict มี token
            {
				list.add(readPosting(indexFile.getChannel(), termDict.get(token)).getList()); // เอา readPosting ที่ข้างในนั้นเอา list มาพร้อมกับเอา token มาใช้กับ indexFile ใส่ลงไปใน list ที่เป็น List<List<Integer>>
			}
	}
	Iterator<List<Integer>> iter = list.iterator(); // สร้าง List ที่สามารถ อ้างอิงข้อมูลได้(Iterator) โดยการดึงจาก list.iterator();
	List<Integer> list1 = popNextOrNull(iter); // สร้าง List ที่เอามาข้อมูลออกหรือทำให้ว่าง
	List<Integer> l2;

	while (list1 != null && (l2 = popNextOrNull(iter)) != null)
		list1 = intersect(list1, l2);
	return list1;
}
```

ฟังก์ชันสุดท้ายคือ 

```java
    String outputQueryResult(List<Integer> res) {
        /*
         * TODO: 
         * 
         * Take the list of documents ID and prepare the search results, sorted by lexicon order. 
         * 
         * E.g.
         * 	0/fine.txt
		 *	0/hello.txt
		 *	1/bye.txt
		 *	2/fine.txt
		 *	2/hello.txt
		 *
		 * If there no matched document, output:
		 * 
		 * no results found
		 *  เราจะต้องเอา docID มาเพื่อหาผลลัพธ์ โดยเรียงตามลำดับ
         * */
    	
    	return null;
    }
```

```java
	String outputQueryResult(List<Integer> res) 
	{
		StringBuilder str = new StringBuilder(""); // สร้าง StringBuilder ขึ้นมา
		if(res == null) // ถ้าใน List นั้นไม่มีค่า Integer หรือ บ๋อแบ๋
        {
			return("NO RESULT FILE"); 
		}
            res.forEach((Integer docId) -> { // นี่คือ for loop โดยใช้ docId เป็นตัวควบคุม โดยถ้าใน List มีค่า Integer 
            str.append(docDict.get(docId)).append("\n"); // ตัว StringBuilder จะทำการเอา docDict ที่มี docId ใส่ (\n มันคือ new line หรือขึ้นบัรรทัดใหม่) ใส่รวมลงไป
            }); 

		return str.toString();
	}
```