# Project 3 PageRank

## Due Date : Monday, 2 December 2019, 11:55 PM

### เราต้องโหลดอะไรบ้าง

-   p3_pagerank.7z
-   p3_testcase.7z
-   1-2019-ITCS414-P3.v1.pdf (เอามาอ่านเถอะ กราบละ)

### Dataset มีอะไรบ้าง
-   citeseer.dat (ในแฟ้ม p3_pagerank.7z) ขนาด ~16MBs
-   test.dat (ในแฟ้ม p3_testcase.7) ขนาด ~50Bytes

## มีกี่ TODO?

6 TODO ที่เราต้องทำ
-   public void loadData(String inputLinkFilename){}
-   public void initialize(){}
-   public double getPerplexity(){return 0;}
-   public boolean isConverge(){return false;}
-   public void runPageRank(String perplexityOutFilename, String prOutFilename){}
-   public Integer[] getRankedPages(int K){return null;}

### ประกาศ HashMap ไว้เป็น global ก่อนใช้
```java
private HashMap<Integer, PRank> Pagemap = new HashMap<>();
private HashMap<Integer, HashSet<Integer>> DatastoreGraph = new HashMap<>();
private double perplexity;
private int Count = 1;
private final int limit = 4;
private static final double factor = 0.85;
```

### TODO ที่ 1 public void loadData(String inputLinkFilename)

ฟังก์ชั่นแรกจะเป็นการโหลดข้อมูลเข้ามาใส่ไว้ในหน่วยความจำ(Memory)ก่อน ซึ่งข้อมูลที่เข้ามานั้นมาในรูปแบบ String โดยเราจะต้องมีการแปลงข้อมูลที่โหลดเข้ามาให้เป็น Integer 

### TODO ที่ 2 public void initialize()

ฟังก์ชั่นที่สองนี้จะถูกเรียกใช้หลังจากเรียกใช้ฟังก์ชั่นแรกสำเร็จแล้ว และจะมีการกำหนดค่าสำหรับ PageRank โดยที่มีการใส่ weight ไว้ให้แต่ละ Page ด้วย

### TODO ที่ 3 public double getPerplexity()

ฟังก์ชั่นที่สามนี้จะคำนวนค่าความสับสน(Perplexity)โดยยึดตาม testcase ที่ให้มา

### TODO ที่ 4 public boolean isConverge()

ฟังก์ชั่นที่สี่นี้จะรีเทิร์นค่าเป็น True และ False ซึ่งรีเทิร์น True ก็ต่อเมื่อค่า perplexity นั้นเกิดการ converge ขึ้น และรีเทิร์น False ถ้าค่า perplexity ไม่ converge

### TODO ที่ 5 public void runPageRank(String perplexityOutFilename, String prOutFilename)

ฟังก์ชั่นที่ห้านี้เป็นฟังก์ชั่นหลักที่สำคัญ ในขณะที่เราเริ่มรันตัวโค้ดนั้นฟังก์ชั่นนี้จะทำการ track ค่า perplexity หลังจากที่รันลูปแบบ iterations แล้ว<br>ค่าที่ได้หน้าตาจะประมาณนี้

```java
// perplexityOutFilename
  /**       183811
    *       79669.9
    *       86267.7
    *       72260.4
    *       75132.4
    **/
// prOutFilename
/*   1   0.1235
 *   2   0.3542
 *   3   0.236
 */
```

### TODO ที่ 6 public Integer[] getRankedPages(int K)

ฟังก์ชั่นนี้จะรีเทิร์นค่า Top K ของ Page ออกมา และค่าคะแนนที่สูงที่สุด

### เริ่มที่ TODO 1 กัน

**ก่อนเริ่มเขียนนั้น ต้องสร้างคลาสเพิ่ม(ข้อดีในการสร้างคลาสคือลดเวลาการเขียนโค้ดได้อีกเยอะ)**<br>ไปเขียนคลาสเพิ่มก่อนแล้วค่อยกลับมาทำ

### TODO 1 แบบสร้าง class ขึ้นมาใหม่
เราจะใช้ Comparable ซึ่ง Comparable เหมือนกับตัวเปรียบเทียบค่าให้อัตโนมัติ
```java
private static class PRANK implements Comparable
{
        private int PageID,count = 0;
        private double PageRank;

        void setPageRank(double pageRank) 
        {
            this.PageRank = pageRank;
        }
        boolean isSinkPage() 
        {
            return count == 0;
        }
        int getId() 
        {
            return PageID;
        }
        double getRank()
        {
            return PageRank;
        }

        int getOutLinkCount() 
        {
            return count;
        }

        PRANK(int id) 
        {
            this(id,0.0);
        }
        PRANK(int id , double PRank)
        {
            this.PageID = id;
            this.PageRank = PRank;
        }
        void incrementOutLinkCount() 
        {
            count++;
        }

        @Override
        public int compareTo(Object o) 
        {
        if(!(o instanceof PRANK))
        {
            return -1 ;
        }
        int Compare = Double.compare(((PRANK) o).PageRank,this.PageRank);
        if(Compare == 0)
        {
            return this.PageID - ((PRANK) o).PageID;
        }
            return Compare;
        }
}
```
ไปที่ฟังก์ชั่นแรกกันเราสามารถเริ่มเขียนโค้ดได้เลย<br>เราจะใช้ File กับ Bufferreader มาใช้ในฟังก์ชั่นนี้
```java
public void loadData(String inputLinkFilename) throws FileNotFoundException, IOException
{
    Charset charset = Charset.forName("UTF-8");
    Stream<String> data;
    data = Files.lines(Paths.get(inputLinkFilename), charset);

        data.forEachOrdered(line -> {
            String[] splits = line.split("\\s");
            int PID = Integer.parseInt(splits[0]);
            if (!Pagemap.containsKey(PID)) 
            {
                Pagemap.put(PID, new PRank(PID));
            }
            HashSet<Integer> Node = new HashSet<>();
            for (int i = 1; i < splits.length; i++) 
            {
                int PageID = Integer.parseInt(splits[i]);

                Node.add(PageID);

                if (!Pagemap.containsKey(PageID)) 
                {
                    Pagemap.put(PageID, new PRANK(PageID));
                }
                Pagemap.get(PageID).incrementOutLinkCount();
            }
            DatastoreGraph.put(PID, Node);
            System.out.println(DatastoreGraph);
        });
}
```

### ต่อไปอีกก็ TODO ที่ 2
เนื่องจากเราสร้าง class เพิ่มแล้วจึงทำให้เราเขียนโค้ดในฟังก์ชั่นนี้น้อยลงกว่าเดิม
```java
public void initialize()
{
    for(Map.Entry<Integer,PRANK> PWeight : Pagemap.entrySet())
    {
        PWeight.getValue().setPageRank(1/(double) Pagemap.size());
    }
}
```
สั้นใช่ไหม ใช่ สั้นจริงๆแหละ 

### ต่อไปอีกนิ๊ดกับ TODO ที่ 3

ฟังก์ชั่นต่อไปนี้ก็จะสั้นคล้ายๆกับ TODO ที่ 2 เช่นกัน

```java
public double getPerplexity()
{
    double entropy = 0.0;
    for(Map.Entry<Integer,PRANK> PageEntry : Pagemap.entryset())
    {
        PRANK pageRank = PageEntry.getValue();
        entropy += pageRank.getRank() * Math.log(pageRank.getRank()) / Math.log(2);
    }
    return Math.pow(2, entropy * -1);
}
```

### ต่อไปอีกหน่อยก็ TODO ที่ 4

ฟังก์ชั่นนี้จะทำการเช็คค่าด้วย boolean แบบปกติว่าค่านี้ converge หรือไม่

```java
public boolean isConverge()
{
    double Newperplexity = getPerplexity();
    if(Math.floor(Newperplexity) % 10 == Math.floor(perplexity) % 10)
    {
        count++
    }
    else
    {
        count = 1;
    }
    this.perplexity = Newperplexity;
    if(Count == limit)
    {
        Count = 1;
        return true;
    }
    return false;
}
```

### ต่อจากนั้นก็ TODO ที่ 5

```java
public void runPageRank(String perplexityOutFilename, String prOutFilename)
{
    int Pagecount = Pagemap.size();
    List<String> perplexity = new ArrayList<>();
    List<String> PageScore = new ArrayList<>();
    while(!isConverge())
    {
        double PageRank = 0.0;
        HashMap<Integer,Double> PageRanks = new HashMap<>();
        for(int PageID : Pagemap.keySet())
        {
            if(Pagemap.get(PageID).isSinkPage())
            {
                PageRank += Pagemap.get(PageID).getRank();
            }
        }
        for(int PageID : Pagemap.keySet())
        {
            double NewPageRank = (1 - factor) / (double) Pagecount;
            NewPageRank += factor * PageRank / (double) Pagecount;
            if(DatastoreGraph.get(PageID) != null && ! DatastoreGraph.get(PageID).isEmpty())
            {
                for(int page: DatastoreGraph.get(PageID))
                {
                    final PRANK newpage = Pagemap.get(page);
                    NewPageRank += factor * newpage.getRank() / (double) newpage.getOutLinkCount();
                }
            }
                PRANK.put(PageID,NewPageRank);
            for(Map.Entry<Integer,PRANK> Page : Pagemap.entrySet())
            {
                Page.getValue().setPageRank(PRANK.get(Page.getKey()));
            }
            perplexity.add(String.valueOf(getPerplexity()));
        }

    for(Map.Entry<Integer,PRANK> Page : Pagemap.entrySet())
        {
            PageScore.add(Page.getKey()+ " " + Page.getValue().getRank());
        }
    }
    WriteFileOut(perplexityOutFilename, perplexity);
    WriteFileOut(prOutFilename, Pagescore);
}
```

### TODO ที่ 6 getRankedPage

ฟังก์ชั่นนี้จะทำการรีเทิร์นค่าของ ***Pagerank*** ออกมาจากคำนวนด้วยการเขียนโค้ดในฟังก์ชั่น

```java
    public Integer[] getRankedPages(int K) {
        ArrayList<PRANK> PArray = new ArrayList<>(Pagemap.values());
        PArray.sort(PRank::compareTo);
        int arrsize = Math.min(K, PArray.size());
        Integer[] topResults = new Integer[arrsize];
        List<PRank> sortResult = PArray.subList(0, arrsize);
        for (int i = 0; i < arrsize; i++) {
            topResults[i] = sortResult.get(i).getId();
        }
        return topResults;
    }
```

### TODO เสริม (อันที่ 7) สำหรับเขียนไฟล์

ฟังก์ชั่นตัวนี้เอาไว้สำหรับเขียนไฟล์ออกมา(Output)

```python
private void WriteFileOut(String file, List<String> line) throws IOException {
        Path pathout = Paths.get(file);
        File newfile = new File(file);

        if (newfile.exists())
        {
            newfile.delete();
        }
        Files.write(pathout, line, StandardCharsets.UTF_8);
    }
```