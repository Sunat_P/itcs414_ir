
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class PageRanketester {

    private HashMap<Integer, PRank> Pagemap = new HashMap<>();
    private HashMap<Integer, HashSet<Integer>> DatastoreGraph = new HashMap<>();
    private double perplexity;
    private int Count = 1;
    private final int limiter = 4;
    private static final double factor = 0.85;

    public void loadData(String inputLinkFilename) throws FileNotFoundException, IOException {
        Charset charset = Charset.forName("UTF-8");
        Stream<String> data;
        data = Files.lines(Paths.get(inputLinkFilename), charset);

        data.forEachOrdered(line -> {
            String[] splits = line.split("\\s");
            int PID = Integer.parseInt(splits[0]);
            if (!Pagemap.containsKey(PID)) {
                Pagemap.put(PID, new PRank(PID));
            }
            HashSet<Integer> Node = new HashSet<>();
            for (int i = 1; i < splits.length; i++) {
                int PageID = Integer.parseInt(splits[i]);

                Node.add(PageID);

                if (!Pagemap.containsKey(PageID)) {
                    Pagemap.put(PageID, new PRank(PageID));
                }
                Pagemap.get(PageID).incrementOutLinkCount();
            }

            DatastoreGraph.put(PID, Node);
            System.out.println(DatastoreGraph);
        });
    }

    public void initialize() {
        for (Map.Entry<Integer, PRank> PWeight : Pagemap.entrySet()) {
            PWeight.getValue().setPageRank(1 / (double) Pagemap.size());
        }

    }

    public double getPerplexity() {
        double entropy = 0;
        for (Map.Entry<Integer, PRank> Pentry : Pagemap.entrySet()) {
            PRank pRank = Pentry.getValue();
            entropy += pRank.getRank() * Math.log(pRank.getRank()) / Math.log(2);
        }
        System.out.println(entropy);
        return Math.pow(2, entropy * -1);
    }

    public boolean isConverge() {
        double Newperplexity = getPerplexity();
        if (Math.floor(Newperplexity) % 10 == Math.floor(perplexity) % 10) {
            Count++;
        } else {
            Count = 1;
        }
        perplexity = Newperplexity;
        if (Count == limiter) {
            Count = 1;
            return true;
        }
        return false;
    }

    public void runPageRank(String perplexityOutFilename, String prOutFilename) throws IOException {
        int CountPage = Pagemap.size();
        List<String> perplexityscore = new ArrayList<>();
        List<String> Pagescore = new ArrayList<>();
        while (!isConverge()) {
            double PageRank = 0;
            HashMap<Integer, Double> PRanks = new HashMap<>();
            for (int PID : Pagemap.keySet()) {
                if (Pagemap.get(PID).isSinkPage()) {
                    PageRank += Pagemap.get(PID).getRank();
                }
            }
            for (int PID : Pagemap.keySet()) {
                double newPRank = (1 - factor) / (double) CountPage;
                newPRank += factor * PageRank / (double) CountPage;
                if (DatastoreGraph.get(PID) != null && !DatastoreGraph.get(PID).isEmpty()) {
                    for (int page : DatastoreGraph.get(PID)) {
                        final PRank newpage = Pagemap.get(page);
                        newPRank += factor * newpage.getRank() / (double) newpage.getOutLinkCount();
                    }
                }
                PRanks.put(PID, newPRank);
            }
            for (Map.Entry<Integer, PRank> Page : Pagemap.entrySet()) {
                Page.getValue().setPageRank(PRanks.get(Page.getKey()));
            }
            perplexityscore.add(String.valueOf(getPerplexity()));
        }
        for (Map.Entry<Integer, PRank> Page : Pagemap.entrySet()) {
            Pagescore.add(Page.getKey() + " " + Page.getValue().getRank());
        }
        WriteFileOut(perplexityOutFilename, perplexityscore);
        WriteFileOut(prOutFilename, Pagescore);
    }

    private void WriteFileOut(String file, List<String> line) throws IOException {
        Path pathout = Paths.get(file);
        File newfile = new File(file);

        if (newfile.exists()) {
            newfile.delete();
        }
        Files.write(pathout, line, StandardCharsets.UTF_8);
    }

    public Integer[] getRankedPages(int K) {
        ArrayList<PRank> PArray = new ArrayList<>(Pagemap.values());
        PArray.sort(PRank::compareTo);
        int arrsize = Math.min(K, PArray.size());
        Integer[] topResults = new Integer[arrsize];
        List<PRank> sortResult = PArray.subList(0, arrsize);
        for (int i = 0; i < arrsize; i++) {
            topResults[i] = sortResult.get(i).getId();
        }
        return topResults;
//            return null;
    }

    public static void main(String args[]) throws IOException {
        long startTime = System.currentTimeMillis();
        PageRanketester pageRanker = new PageRanketester();
        pageRanker.loadData("test/test.dat");
        pageRanker.initialize();
        pageRanker.runPageRank("perplexity1.out", "pr_scores1.out");
        Integer[] rankedPages = pageRanker.getRankedPages(100);
        double estimatedTime = (double) (System.currentTimeMillis() - startTime) / 1000.0;

        System.out.println("Top 100 Pages are:\n" + Arrays.toString(rankedPages));
        System.out.println("Proccessing time: " + estimatedTime + " seconds");
    }

    private static class PRank implements Comparable {

        private int PageID, count = 0;
        private double PageRank;

        void setPageRank(double pageRank) {
            this.PageRank = pageRank;
        }

        boolean isSinkPage() {
            return count == 0;
        }

        int getId() {
            return PageID;
        }

        double getRank() {
            return PageRank;
        }

        int getOutLinkCount() {
            return count;
        }

        PRank(int id) {
            this(id, 0.0);
        }

        PRank(int id, double PRank) {
            this.PageID = id;
            this.PageRank = PRank;
        }

        void incrementOutLinkCount() {
            count++;
        }

        @Override
        public int compareTo(Object o) {
            if (o instanceof PRank) {
            } else {
                return -1;
            }
            int Compare;
            Compare = Double.compare(((PRank) o).PageRank, this.PageRank);
            if (Compare == 0) {
                return this.PageID - ((PRank) o).PageID;
            }
            return Compare;
        }
    }
}
